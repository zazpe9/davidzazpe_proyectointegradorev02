<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Bike_Category
 *
 * @ORM\Table(name="bikes__categories")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Bikes_CategoriesRepository")
 */
class Bikes_Categories {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="category", type="string", length=45)
     */
    private $category;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set category
     *
     * @param string $category
     *
     * @return Bikes_Categories
     */
    public function setCategory($category) {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory() {
        return $this->category;
    }

    /**
     * @ORM\OneToMany(targetEntity="Bike", mappedBy="category")
     */
    private $bikes;

    public function __construct() {
        $this->bikes = new ArrayCollection();
    }
    
    public function __toString() {
        return $this->category;
    }


}
