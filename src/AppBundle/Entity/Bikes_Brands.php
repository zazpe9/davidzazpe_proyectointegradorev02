<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Bikes_Brands
 *
 * @ORM\Table(name="bikes__brands")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Bikes_BrandsRepository")
 */
class Bikes_Brands {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="brand", type="string", length=45)
     */
    private $brand;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255)
     */
    private $image;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set brand
     *
     * @param string $brand
     *
     * @return Bikes_Brands
     */
    public function setBrand($brand) {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return string
     */
    public function getBrand() {
        return $this->brand;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Bikes_Brands
     */
    public function setImage($image) {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * @ORM\OneToMany(targetEntity="Bike", mappedBy="brand")
     */
    private $bikes;

    public function __construct() {
        $this->bikes = new ArrayCollection();
    }
    
    public function __toString() {
        return $this->brand;
    }



}
