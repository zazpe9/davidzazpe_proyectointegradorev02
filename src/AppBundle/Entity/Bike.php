<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Bikes_Categories;
use AppBundle\Entity\Bikes_Brands;

/**
 * Bikes
 *
 * @ORM\Table(name="bikes")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BikesRepository")
 */
class Bike {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=45)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255)
     */
    private $image;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Bike
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Bike
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Bike
     */
    public function setImage($image) {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * @ORM\ManyToOne(targetEntity="Bikes_Categories", inversedBy="bikes")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;
    
    /**
     * @ORM\ManyToOne(targetEntity="Bikes_Brands", inversedBy="bikes")AppBikes
     * @ORM\JoinColumn(name="brand_id", referencedColumnName="id")
     */
    private $brand;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="bikes")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    function getCategory() {  
        return $this->category;
    }

    function getBrand() {
        return $this->brand;
    }

    function getUser() {
        return $this->user;
    }

    function setCategory($category) {
        $this->category = $category;
    }

    function setBrand($brand) {
        $this->brand = $brand;
    }

    function setUser($user) {
        $this->user = $user;
    }

}
