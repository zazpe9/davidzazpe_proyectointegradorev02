<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Bikes_Brands;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Bikes_brand controller.
 *
 * @Route("bikes_brands")
 */
class Bikes_BrandsController extends Controller
{
    /**
     * Lists all bikes_Brand entities.
     *
     * @Route("/", name="bikes_brands_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $bikes_Brands = $em->getRepository('AppBundle:Bikes_Brands')->findAll();

        return $this->render('bikes_brands/index.html.twig', array(
            'bikes_Brands' => $bikes_Brands,
        ));
    }

    /**
     * Creates a new bikes_Brand entity.
     *
     * @Route("/new", name="bikes_brands_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $bikes_Brand = new Bikes_Brands();
        $form = $this->createForm('AppBundle\Form\Bikes_BrandsType', $bikes_Brand);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($bikes_Brand);
            $em->flush();

            return $this->redirectToRoute('bikes_brands_show', array('id' => $bikes_Brand->getId()));
        }

        return $this->render('bikes_brands/new.html.twig', array(
            'bikes_Brand' => $bikes_Brand,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a bikes_Brand entity.
     *
     * @Route("/{id}", name="bikes_brands_show")
     * @Method("GET")
     */
    public function showAction(Bikes_Brands $bikes_Brand)
    {
        $deleteForm = $this->createDeleteForm($bikes_Brand);

        return $this->render('bikes_brands/show.html.twig', array(
            'bikes_Brand' => $bikes_Brand,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing bikes_Brand entity.
     *
     * @Route("/{id}/edit", name="bikes_brands_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Bikes_Brands $bikes_Brand)
    {
        $deleteForm = $this->createDeleteForm($bikes_Brand);
        $editForm = $this->createForm('AppBundle\Form\Bikes_BrandsType', $bikes_Brand);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('bikes_brands_edit', array('id' => $bikes_Brand->getId()));
        }

        return $this->render('bikes_brands/edit.html.twig', array(
            'bikes_Brand' => $bikes_Brand,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a bikes_Brand entity.
     *
     * @Route("/{id}", name="bikes_brands_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Bikes_Brands $bikes_Brand)
    {
        $form = $this->createDeleteForm($bikes_Brand);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($bikes_Brand);
            $em->flush();
        }

        return $this->redirectToRoute('bikes_brands_index');
    }

    /**
     * Creates a form to delete a bikes_Brand entity.
     *
     * @param Bikes_Brands $bikes_Brand The bikes_Brand entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Bikes_Brands $bikes_Brand)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('bikes_brands_delete', array('id' => $bikes_Brand->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
