<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Bike;
use AppBundle\Entity\Bikes_Brands;
use AppBundle\Entity\Bikes_Categories;
use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
      $bikes = $this->getDoctrine()
        ->getRepository(Bike::class)
        ->findAllActive();

      return $this->render('default/index.html.twig', array(
        'bikes' => $bikes,
      ));
    }
}
