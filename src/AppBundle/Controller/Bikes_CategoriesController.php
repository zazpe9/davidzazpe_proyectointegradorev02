<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Bikes_Categories;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Bikes_category controller.
 *
 * @Route("bikes_categories")
 */
class Bikes_CategoriesController extends Controller
{
    /**
     * Lists all bikes_Category entities.
     *
     * @Route("/", name="bikes_categories_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $bikes_Categories = $em->getRepository('AppBundle:Bikes_Categories')->findAll();

        return $this->render('bikes_categories/index.html.twig', array(
            'bikes_Categories' => $bikes_Categories,
        ));
    }

    /**
     * Creates a new bikes_Category entity.
     *
     * @Route("/new", name="bikes_categories_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $bikes_Category = new Bikes_Categories();
        $form = $this->createForm('AppBundle\Form\Bikes_CategoriesType', $bikes_Category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($bikes_Category);
            $em->flush();

            return $this->redirectToRoute('bikes_categories_show', array('id' => $bikes_Category->getId()));
        }

        return $this->render('bikes_categories/new.html.twig', array(
            'bikes_Category' => $bikes_Category,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a bikes_Category entity.
     *
     * @Route("/{id}", name="bikes_categories_show")
     * @Method("GET")
     */
    public function showAction(Bikes_Categories $bikes_Category)
    {
        $deleteForm = $this->createDeleteForm($bikes_Category);

        return $this->render('bikes_categories/show.html.twig', array(
            'bikes_Category' => $bikes_Category,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing bikes_Category entity.
     *
     * @Route("/{id}/edit", name="bikes_categories_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Bikes_Categories $bikes_Category)
    {
        $deleteForm = $this->createDeleteForm($bikes_Category);
        $editForm = $this->createForm('AppBundle\Form\Bikes_CategoriesType', $bikes_Category);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('bikes_categories_edit', array('id' => $bikes_Category->getId()));
        }

        return $this->render('bikes_categories/edit.html.twig', array(
            'bikes_Category' => $bikes_Category,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a bikes_Category entity.
     *
     * @Route("/{id}", name="bikes_categories_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Bikes_Categories $bikes_Category)
    {
        $form = $this->createDeleteForm($bikes_Category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($bikes_Category);
            $em->flush();
        }

        return $this->redirectToRoute('bikes_categories_index');
    }

    /**
     * Creates a form to delete a bikes_Category entity.
     *
     * @param Bikes_Categories $bikes_Category The bikes_Category entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Bikes_Categories $bikes_Category)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('bikes_categories_delete', array('id' => $bikes_Category->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
