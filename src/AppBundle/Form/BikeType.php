<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use AppBundle\Entity\Bikes_Categories;
use AppBundle\Entity\Bikes_Brands;
use AppBundle\Entity\User;
use AppBundle\Repository\Bikes_BrandsRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Repository\Bikes_CategoriesRepository;
use AppBundle\Repository\UsersRepository;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class BikeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, [
        'label' => 'Nombre: ',
        'attr' => ['class' => 'form-control'],
      ])
      ->add('image', TextType::class, [
        'label' => 'Logo: ',
        'attr' => ['class' => 'form-control'],
      ])
      ->add('description', TextType::class, [
        'label' => 'Descripción: ',
        'attr' => ['class' => 'form-control'],
      ])
      ->add('category', EntityType::class, [
        'label' => 'Categoría: ',
        'attr' => ['class' => 'form-control'],
        'class' => Bikes_Categories::class,
        'query_builder' => function(Bikes_CategoriesRepository  $r)  {
         return $r->getBikes_CategoriesQueryBuilder();}
      ])
         
      ->add('brand', EntityType::class, [
        'label' => 'Marca: ',
        'attr' => ['class' => 'form-control'],
        'class' => Bikes_Brands::class,
        'query_builder' => function(Bikes_BrandsRepository $r)  {
         return $r->getBikes_BrandsQueryBuilder();}
      ])
      ->add('user', EntityType::class, [
        'label' => 'Usuario: ',
        'attr' => ['class' => 'form-control'],
        'class' => User::class,
        'query_builder' => function(UsersRepository $r)  {
         return $r->getUsersQueryBuilder();}
      ])

      ->add('save', SubmitType::class, array('label' => 'Guardar',  'attr' => array('class'=>'btn btn-primary')));
  }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Bike'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_bike';
    }


}
