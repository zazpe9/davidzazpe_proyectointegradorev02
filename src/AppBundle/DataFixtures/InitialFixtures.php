<?php
namespace AppBundle\DataFixtures;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use AppBundle\Entity\Bikes_Brands;
use AppBundle\Entity\Bikes_Categories;
use AppBundle\Entity\Bike;
use AppBundle\Entity\Role;
use AppBundle\Entity\User;

class InitialFixtures implements ORMFixtureInterface{
    
    public function load(ObjectManager $manager){
        $faker = Faker\Factory::create();
        $role1 = new Role();
        $role1->setRole("Administrator");
        $manager->persist($role1);
        
        $role2 = new Role();
        $role2->setRole("Default");
        $manager->persist($role2);
        
        $brand1 = new Bikes_Brands();
        $brand1->setBrand("Orbea");
        $brand1->setImage($faker->imageUrl());
        $manager->persist($brand1);
                
        $brand2 = new Bikes_Brands();
        $brand2->setBrand("BH");
        $brand2->setImage($faker->imageUrl());
        $manager->persist($brand2);
        
        $brand3 = new Bikes_Brands();
        $brand3->setBrand("Conor");
        $brand3->setImage($faker->imageUrl());
        $manager->persist($brand3);
                   
        $category1 = new Bikes_Categories();
        $category1->setCategory("Montaña");
        $manager->persist($category1);
        
        $category2 = new Bikes_Categories();
        $category2->setCategory("Ciudad");
        $manager->persist($category2);
                              
        
        // Creando Usuarios Administradores
        for($i=0;$i<10;$i++){
            $userFaker1 = Faker\Factory::create();
            $u1 = new User();
            $u1->setUsername($userFaker1->userName);
            $u1->setEmail($userFaker1->email);
            $u1->setPassword($userFaker1->password);
            $u1->setRole($role1);
            $manager->persist($u1);
        }
        
        // Creando Usuarios Defaults
        for($i=0;$i<10;$i++){
            $userFaker2 = Faker\Factory::create();
            $u2 = new User();
            $u2->setUsername($userFaker2->userName);
            $u2->setEmail($userFaker2->email);
            $u2->setPassword($userFaker2->password);
            $u2->setRole($role2);
            $manager->persist($u2);
        }
        
        $manager->flush();
        
        // Creando Bicis Monte
        for($i=0;$i<5;$i++){
            $u = $manager->getRepository('AppBundle\Entity\User')->find($i);
            $bikeFaker = Faker\Factory::create();
            $b = new Bike();
            $b->setBrand($brand1);
            $b->setName($bikeFaker->word);
            $b->setCategory($category1);
            $b->setDescription($bikeFaker->sentence);
            $b->setImage($bikeFaker->imageUrl());
            $b->setUser($u);
            $manager->persist($b);
            
        }
        
        for($i=0;$i<5;$i++){
            $u = $manager->getRepository('AppBundle\Entity\User')->find($i+5);
            $bikeFaker1 = Faker\Factory::create();
            $b1 = new Bike();
            $b1->setBrand($brand3);
            $b1->setName($bikeFaker1->word);
            $b1->setCategory($category1);
            $b1->setDescription($bikeFaker1->sentence);
            $b1->setImage($bikeFaker1->imageUrl());
            $b1->setUser($u);
            $manager->persist($b1);
            
        }
        
        // Creando Bicis Ciudad
        for($i=0;$i<5;$i++){
            $u = $manager->getRepository('AppBundle\Entity\User')->find($i+10);
            $bikeFaker2 = Faker\Factory::create();
            $b2 = new Bike();
            $b2->setBrand($brand2);
            $b2->setName($bikeFaker2->word);
            $b2->setCategory($category2);
            $b2->setDescription($bikeFaker2->sentence);
            $b2->setImage($bikeFaker2->imageUrl());
            $b2->setUser($u);
            $manager->persist($b2);
        }
        
        $manager->flush();
         
    }
     
}

?>
